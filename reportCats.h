///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07d - Aminal Farm 1 - EE 205 - Spr 2022
/////
///// @file deleteCats.h
///// @version 1.0
/////
///// Configuration header file for the Delete Cats module
/////
///// @author Byron Soriano <byrongs@hawaii.edu>
///// @date   19_Feb_2022
/////////////////////////////////////////////////////////////////////////////////
//
#pragma once
#include "catDatabase.h"
extern void   printCat( const size_t index ) ;
extern void   printAllCats() ;
extern size_t findCat( const char* name ) ;  /// @returns the index or BAD_CAT

extern const char* genderName( const enum Gender gender ) ;

extern const char* breedName( const enum Breed breed ) ;

extern const char* colorName (const enum Color color ) ;

