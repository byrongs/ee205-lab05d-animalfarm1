///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07d - Aminal Farm 1 - EE 205 - Spr 2022
/////
///// @file deleteCats.c
///// @version 1.0
/////
///// This modules deletes cats in the catabase.
/////
///// @author Byron Soriano <byrongss@hawaii.edu>
///// @date   07_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdbool.h>

#include "deleteCats.h"
#include "catDatabase.h"

bool deleteCat( const size_t index ) {
	if( !isIndexValid( index ) ) {
		fprintf( stderr, "%s: %s(): Bad cat!\n", PROGRAM_NAME, __FUNCTION__ ) ;
		return false;
	}

	if( numCats == 0 ) {
		return true;
	}

	swapCat( index, numCats-1 ) ;

	wipeCat( numCats-1);

	numCats -= 1;

	#ifdef DEBUG
	       printf( "%s: %s: Cat [%lu] has been deleted.  There are [%lu] in the database.\n", PROGRAM_NAME, __FUNCTION__, index, numCats );
	#endif
	return true;
}

bool deleteAllCats() {
	while( numCats != 0 ) {
		deleteCat( 0 );
	}

	#ifdef DEBUG
		printf(  "%s: %s: All cats have been deleted\n", PROGRAM_NAME, __FUNCTION__ );
        #endif
	
	return true;

}


