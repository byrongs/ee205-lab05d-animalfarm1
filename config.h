///////////////////////////////////////////////////////////////////////////////
/////         University of Hawaii, College of Engineering
///// @brief  Lab 07d - Aminal Farm 1 - EE 205 - Spr 2022
/////
///// @file config.h
///// @version 1.0
/////
///// Configuration header file for Animal Farm
/////
///// @author Byron Soriano <byrongs@hawaii.edu>
///// @date   07_Feb_2022
/////////////////////////////////////////////////////////////////////////////////
#pragma once



#include <limits.h>

#define PROGRAM_TITLE "Animal Farm 1"
#define PROGRAM_NAME  "animalfarm1"

#define MAX_CATS (128)

#define BAD_CAT UINT_MAX

