###############################################################################
####         University of Hawaii, College of Engineering
####  @brief  Lab 07d - Aminal Farm 1 - EE 205 - Spr 2022
####
####  @file Makefile
####  @version 1.0
####  Compiles the entire program.  
#### 
####    
####
####  @author Byron Soriano <byrongs@hawaii.edu>
####  @date   19_Feb_2022
###############################################################################

CC = gcc
CFLAGS = -g -Wall -Wextra $(DEBUG_FLAGS)

TARGET = animalFarm1

all = $(TARGET)


catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

addCats.o: addCats.c catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

deleteCats.o: deleteCats.c deleteCats.h
	$(CC) $(CFLAGS) -c deleteCats.c


updateCats.o: updateCats.c updateCats.h
	$(CC) $(CFLAGS) -c updateCats.c

reportCats.o: reportCats.c reportCats.h
	$(CC) $(CFLAGS) -c  reportCats.c


main.o: main.c config.h catDatabase.h addCats.h reportCats.h updateCats.h deleteCats.h
	$(CC) $(CFLAGS) -c main.c

animalFarm1: main.o catDatabase.o addCats.o deleteCats.o reportCats.o updateCats.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o catDatabase.o addCats.o deleteCats.o reportCats.o updateCats.o

test: $(TARGET)
	./$(TARGET)
clean:
	rm -f $(TARGET) *.o
